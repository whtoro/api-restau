import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MesaRepository } from './mesa.repository';
import { MesaService } from './mesa.service';
import { MesaController } from './mesa.controller';
import { SharedModule } from '../../shared/shared.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MesaRepository]),
    SharedModule,
    AuthModule,
  ],
  providers: [MesaService],
  controllers: [MesaController],
})
export class MesaModule {}
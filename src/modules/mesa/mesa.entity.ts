import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn, ManyToOne, OneToMany
  } from 'typeorm';
  import { Order } from '../order/order.entity';
  
  @Entity('mesa')
  export class Mesa extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;
  
    @Column({ type: 'varchar2', length: 20, nullable: false })
    name: string;
  
    @Column({ type: 'number', nullable: false })
    capacidad: number;

    @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
    createdAt: Date;
  
    @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
    updatedAt: Date;

    @OneToMany(() => Order, order => order.id)
    order: Order[];
  }
  
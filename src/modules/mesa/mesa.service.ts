import {
    Injectable,
    BadRequestException,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mesa } from './mesa.entity';
import { getConnection, Repository } from 'typeorm';
import { MesaRepository } from '../mesa/mesa.repository';
import { status } from '../../shared/entity-status.num';

@Injectable()
export class MesaService {
    constructor(
        @InjectRepository(MesaRepository)
        private readonly _mesaRepository: MesaRepository,
    ){}
    async get(id: number): Promise<Mesa> {
        if (!id) {
            throw new BadRequestException('id must be sent');
        }

        const mesa: Mesa = await this._mesaRepository.findOne(id);

        if (!mesa) {
            throw new NotFoundException();
        }

        return mesa;
    }
    async getAll(): Promise<Mesa[]> {
        const orders: Mesa[] = await this._mesaRepository.find();
        return orders;
    }
    async create(order: Mesa): Promise<Mesa> {
        const savedMesa: Mesa = await this._mesaRepository.save(order);
        return savedMesa;
    }
}

import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Patch,
    Delete,
    ParseIntPipe,
  } from '@nestjs/common';
  import { MesaService } from './mesa.service';
  import { Mesa } from './mesa.entity';
  
  @Controller('mesa')
  export class MesaController {
    constructor(private readonly _mesaService: MesaService) { }
  
    @Get(':id')
    async getMesa(@Param('id', ParseIntPipe) id: number): Promise<Mesa> {
      const mesa = await this._mesaService.get(id);
      return mesa;
    }
  
    @Get()
    async getMesas(): Promise<Mesa[]> {
      const mesas = await this._mesaService.getAll();
      return mesas;
    }
  
    @Post()
    async createMesa(@Body() mesa: Mesa): Promise<Mesa> {
      const createdMesa = await this._mesaService.create(mesa);
      return createdMesa;
    }
  }
  
import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Patch,
    Delete,
    ParseIntPipe,
  } from '@nestjs/common';
  import { PlatoService } from './plato.service';
  import { Plato } from './plato.entity';
  
  @Controller('platos')
  export class PlatoController {
    constructor(private readonly _platoService: PlatoService) { }
  
    @Get(':id')
    async getPlato(@Param('id', ParseIntPipe) id: number): Promise<Plato> {
      const plato = await this._platoService.get(id);
      return plato;
    }
  
    @Get()
    async getPlatos(): Promise<Plato[]> {
      const platos = await this._platoService.getAll();
      return platos;
    }
  
    @Post()
    async createPlato(@Body() plato: Plato): Promise<Plato> {
      const createdPlato = await this._platoService.create(plato);
      return createdPlato;
    }
  }
  
import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
  } from 'typeorm';
  import { Order } from '../order/order.entity';
  
  @Entity('plato')
  export class Plato extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;
  
    @Column({ type: 'varchar2', length: 20, nullable: false })
    name: string;
  
    @Column({ type: 'number', nullable: false })
    price: number;
  
    @ManyToMany(type => Order, order => order.platos)
    @JoinColumn()
    orders: Order[];
  
    @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
    createdAt: Date;
  
    @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
    updatedAt: Date;
  }
  
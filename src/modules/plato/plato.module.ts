import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlatoRepository } from './plato.repository';
import { PlatoService } from './plato.service';
import { PlatoController } from './plato.controller';
import { SharedModule } from '../../shared/shared.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PlatoRepository]),
    SharedModule,
    AuthModule,
  ],
  providers: [PlatoService],
  controllers: [PlatoController],
})
export class PlatoModule {}
import { Repository, EntityRepository } from 'typeorm';
import { Plato } from './plato.entity';

@EntityRepository(Plato)
export class PlatoRepository extends Repository<Plato> {}

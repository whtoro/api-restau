import {
    Injectable,
    BadRequestException,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Plato } from './plato.entity';
import { getConnection, Repository } from 'typeorm';
import { PlatoRepository } from '../plato/plato.repository';
import { status } from '../../shared/entity-status.num';

@Injectable()
export class PlatoService {
    constructor(
        @InjectRepository(PlatoRepository)
        private readonly _platoRepository: PlatoRepository,
    ){}
    async get(id: number): Promise<Plato> {
        if (!id) {
            throw new BadRequestException('id must be sent');
        }

        const plato: Plato = await this._platoRepository.findOne(id);

        if (!plato) {
            throw new NotFoundException();
        }

        return plato;
    }
    async getAll(): Promise<Plato[]> {
        const orders: Plato[] = await this._platoRepository.find();
        return orders;
    }
    async create(order: Plato): Promise<Plato> {
        const savedPlato: Plato = await this._platoRepository.save(order);
        return savedPlato;
    }
}

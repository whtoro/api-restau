import { User } from '../user/user.entity';
import { Repository, EntityRepository, getConnection } from 'typeorm';
import { SignupDto } from './dto';
import { RoleRepository } from '../role/role.repository';
import { Role } from '../role/role.entity';
import { RoleType } from '../role/roletype.enum';
import { genSalt, hash } from 'bcryptjs';

@EntityRepository(User)
export class AuthRepository extends Repository<User> {
  async signup(signupDto: SignupDto) {
    const { username, email, password, name, lastname } = signupDto;
    const user = new User();
    user.username = username;
    user.email = email;
    user.name = name;
    user.lastname = lastname;

    const roleRepository: RoleRepository = await getConnection().getRepository(
      Role,
    );

    const defaultRole: Role = await roleRepository.findOne({
      where: { name: RoleType.GENERAL },
    });

    user.roles = [defaultRole];

    const salt = await genSalt(10);
    user.password = await hash(password, salt);

    await user.save();
  }
}

import { RoleType } from '../role/roletype.enum';

export interface IJwtPayload {
  id: number;
  username: string;
  name: string;
  lastname: string;
  email: string;
  roles: RoleType[];
  iat?: Date;
}

import {
    Injectable,
    BadRequestException,
    NotFoundException,
  } from '@nestjs/common';
  import { InjectRepository } from '@nestjs/typeorm';
  import { Order } from './order.entity';
  import { getConnection, Repository } from 'typeorm';
  import { PlatoRepository } from '../plato/plato.repository';
  import { OrderRepository } from '../order/order.repository';
  import { status } from '../../shared/entity-status.num';
  
  @Injectable()
  export class OrderService {
    constructor(
      @InjectRepository(OrderRepository)
      private readonly _orderRepository: OrderRepository,
      @InjectRepository(PlatoRepository)
      private readonly _platoRepository: PlatoRepository,
    ) {}
  
    async get(id: number): Promise<Order> {
      if (!id) {
        throw new BadRequestException('id must be sent');
      }
  
      const order: Order = await this._orderRepository.findOne(id);
  
      if (!order) {
        throw new NotFoundException();
      }
  
      return order;
    }
  
    async getAll(): Promise<Order[]> {
      const orders: Order[] = await this._orderRepository.find();
      return orders;
    }
  
    async create(order: Order): Promise<Order> {
      const savedOrder: Order = await this._orderRepository.save(order);
      return savedOrder;
    }
  
    async setPlatoToOrder(orderId: number, platoId: number) {
      const orderExist = await this._orderRepository.findOne(orderId);
  
      if (!orderExist) {
        throw new NotFoundException();
      }
  
      const platoExist = await this._platoRepository.findOne(platoId);
  
      if (!platoExist) {
        throw new NotFoundException('Plato does not exist');
      }
  
      orderExist.platos.push(platoExist);
      await this._orderRepository.save(orderExist);
      
      return true;
    }
  }
  
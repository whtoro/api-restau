import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Patch,
    Delete,
    ParseIntPipe,
    UseGuards,
  } from '@nestjs/common';
  import { OrderService } from './order.service';
  import { Order } from './order.entity';
  import { AuthGuard } from '@nestjs/passport';
  import { Roles } from '../role/decorators/role.decorator';
  import { RoleGuard } from '../role/guards/role.guard';
  
  @Controller('orders')
  export class OrderController {
    constructor(private readonly _orderService: OrderService) {}
  
    @Get(':id')
    //@Roles('ADMINISTRATOR')
    //@UseGuards(AuthGuard(), RoleGuard)
    async getOrder(@Param('id', ParseIntPipe) id: number): Promise<Order> {
      const order = await this._orderService.get(id);
      return order; 
    }
  
    //@UseGuards(AuthGuard())
    @Get()
    async getOrders(): Promise<Order[]> {
      const orders = await this._orderService.getAll();
      return orders;
    }
  
    @Post()
    async createOrder(@Body() order: Order): Promise<Order> {
      const createdOrder = await this._orderService.create(order);
      return createdOrder;
    }
  
    @Post('setPlato/:orderId/:platoId')
    async setRoleToOrder(
      @Param('orderId', ParseIntPipe) orderId: number,
      @Param('platoId', ParseIntPipe) platoId: number,
    ) {
      return this._orderService.setPlatoToOrder(orderId, platoId);
    }
  }
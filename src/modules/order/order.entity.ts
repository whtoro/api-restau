import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToOne,
    JoinColumn,
    JoinTable,
    ManyToMany,
    UpdateDateColumn,
    CreateDateColumn,
    ManyToOne
  } from 'typeorm';
  import { Mesa } from '../mesa/mesa.entity';
  import { Plato } from '../plato/plato.entity';
  
  @Entity('order')
  export class Order extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;
  
    @Column({ type: 'varchar2', length: 25, nullable: false })
    id_user: number;
  
    @ManyToMany(type => Plato, plato => plato.orders, { eager: true })
    @JoinTable({ name: 'order_plato' })
    platos: Plato[];

    @ManyToOne(() => Mesa, Mesa => Mesa.id)
    mesa: Mesa;

    @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
    createdAt: Date;
  
    @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
    updatedAt: Date;
  }
  
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Configuration } from './config/config.keys';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './modules/user/user.module';
import { RoleModule } from './modules/role/role.module';
import { OrderController } from './modules/order/order.controller';
import { OrderModule } from './modules/order/order.module';
import { PlatoService } from './modules/plato/plato.service';
import { PlatoModule } from './modules/plato/plato.module';
import { MesaModule } from './modules/mesa/mesa.module';

@Module({
  imports: [ConfigModule, DatabaseModule, UserModule, RoleModule, OrderModule, PlatoModule, MesaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly _configService: ConfigService){
    AppModule.port = this._configService.get(Configuration.PORT);
  }
}

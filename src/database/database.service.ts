import { TypeOrmModule } from '@nestjs/typeorm';
import { Configuration } from 'src/config/config.keys';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';
import { ConnectionOptions } from 'typeorm';

export const databaseProviders = [
    TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        async useFactory(config: ConfigService){
            return {
                type: 'oracle' as 'oracle',
                connectString: "(DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=oracle12c-slim-demo-container) (PORT=1521) ) (CONNECT_DATA=(SERVICE_NAME=ORCLCDB.localdomain) ))",
                username: config.get(Configuration.USERNAME),
                port: 1521,
                password: config.get(Configuration.PASSWORD),
                entities: [__dirname + '/../**/*.entity{.ts,.js}'],
                migrations: [__dirname + '/migrations/*{.ts,.js}'],
            } as ConnectionOptions
        }
    }),
]